import init, { convert } from "./to_avif.js"

await init()

self.onmessage = async (msg) => {
	const data = msg.data
	const id = Math.random().toString(36).slice(2)
	const start = Date.now()
	console.log({ id, start: new Date(start) })
	const u8 = await encodeAvif(data.buffer, data.speed, data.quality)
	const end = Date.now()
	const elapsed = +((end - start) / 1000).toFixed(3)
	console.log({ id, end: new Date(end), elapsed })

	if (u8) {
		const avif = u8.buffer
		self.postMessage({ type: "result", values: { avif, elapsed } }, [avif])
	} else {
		self.postMessage({ type: "result", values: { elapsed } })
	}
}

const encodeAvif = async (buffer, speed, quality) => {
	const u8 = new Uint8Array(buffer)

	let result
	try {
		result = convert(u8, speed, quality)
	} catch (err) {
		console.error(err)
		return null
	}

	return result
}

self.postMessage({ type: "ready" })
