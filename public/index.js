import van from "https://cdn.jsdelivr.net/gh/vanjs-org/van/public/van-1.4.1.min.js"
import { convertSingle, convertMultiple, scanDir } from "./avif.js"

const { div, span, table, thead, tbody, tr, th, td, label, input, button, hr } = van.tags

const Page = () => {
	const speed = van.state(7)
	const quality = van.state(70)

	const running = van.state(false)

	const filter_jpg = van.state(true)
	const filter_png = van.state(true)
	const filter_gif = van.state(true)
	const filter_bmp = van.state(true)
	const filter_webp = van.state(true)

	const list_items = van.state([])

	const onClickSelectFile = async () => {
		const result = await convertSingle({
			speed: speed.val,
			quality: quality.val,
		})
		if (result?.message) {
			alert(result.message)
		}
	}

	const onClickSelectDir = async () => {
		const result = await scanDir({
			jpg: filter_jpg.val,
			png: filter_png.val,
			gif: filter_gif.val,
			bmp: filter_bmp.val,
			webp: filter_webp.val,
		})

		list_items.val = result.map(item => {
			return van.state({
				checked: true,
				status: "",
				source_size: "",
				result_size: "",
				elapsed: "",
				item,
			})
		})
	}

	const onClickConvert = async () => {
		running.val = true
		const result = await convertMultiple({
			speed: speed.val,
			quality: quality.val,
			items: list_items.val.filter(row => row.val.checked).map(row => row.val.item),
			updateRow: (item, values) => {
				const row = list_items.val.find(row => {
					return row.val.item === item
				})
				row.val = {
					...row.val,
					status: "",
					source_size: "",
					result_size: "",
					elapsed: "",
					...values,
				}
			}
		}).finally(() => {
			running.val = false
		})

		if (result?.message) {
			alert(result.message)
		}
	}

	return div(
		{ style: "padding: 12px;" },
		div(
			{ class: "row" },
			div(
				div(
					{ style: "font-size: 16px; font-weight: bold" },
					"AVIF Converter"
				),
				div(
					{ style: "margin-top: 20px" },
					button({ onclick: onClickSelectFile }, "ファイルを選択"),
				),
			),
			div(
				{ style: "margin-left: 30px" },
				table(
					tbody(
						tr(
							td("Speed"),
							td(
								input({
									type: "range",
									min: 1,
									max: 10,
									value: speed,
									oninput: (eve) => speed.val = eve.target.value,
								}),
								div(
									{ class: "lr small" },
									span("slow"),
									span("fast"),
								),
							),
							td(speed),
						),
						tr(
							td("Quality"),
							td(
								input({
									type: "range",
									min: 1,
									max: 100,
									value: quality,
									oninput: (eve) => quality.val = eve.target.value,
								}),
								div(
									{ class: "lr small" },
									span("low"),
									span("high"),
								),
							),
							td(quality),
						),
					),
				),
			),
		),
		hr({ style: "margin: 20px 0" },),
		div(
			div(
				{ class: "lr" },
				div(
					div(
						button({ onclick: onClickSelectDir, disabled: running }, "フォルダを選択"),
					),
					div(
						{ style: "margin-top: 5px; display: flex; gap: 10px" },
						Check({ state: filter_jpg, label: ".jpg" }),
						Check({ state: filter_png, label: ".png" }),
						Check({ state: filter_gif, label: ".gif" }),
						Check({ state: filter_bmp, label: ".bmp" }),
						Check({ state: filter_webp, label: ".webp" }),
					),
				),
				div(
					button({ onclick: onClickConvert, disabled: running }, "変換")
				)
			),
			div(
				{ style: "margin-top: 10px; max-height: 50vh; overflow: auto; border: 1px solid #bbb" },
				() => {
					return table(
						{ class: "grid-table", style: "width: 100%" },
						thead(
							{ style: "position: sticky; top: 0; background: #ffefe0" },
							tr(
								th(
									{ style: "width: 20px; max-width: 20px" },
									input({
										type: "checkbox",
										checked: () => list_items.val.every(row => {
											return row.val.checked
										}),
										onchange: (event) => {
											for (const row of list_items.val) {
												row.val = {
													...row.val,
													checked: event.target.checked
												}
											}
										},
									})
								),
								th("Path"),
								th("Type"),
								th("Status"),
								th("Source size"),
								th("Result size"),
								th("elapsed"),
							)
						),
						tbody(
							list_items.val.map(row => {
								return () => Row({ row })
							})
						),
					)
				}
			)
		)
	)
}

const Row = ({ row }) => {
	const item = row.val.item
	const path = item.path
	const type = (item.handle.name.split(".").slice(1).pop() || "").toUpperCase()
	return tr(
		td(
			input({
				style: "text-align: center",
				type: "checkbox",
				checked: () => row.val.checked,
				onchange: (event) => {
					row.val = {
						...row.val,
						checked: event.target.checked
					}
				},
			}),
		),
		td(path),
		td(type),
		td(row.val.status),
		td({ style: "text-align: right" }, row.val.source_size),
		td({ style: "text-align: right" }, row.val.result_size),
		td({ style: "text-align: right" }, row.val.elapsed),
	)
}

const Check = ({ label: _label, state }) => {
	return label(
		input({
			type: "checkbox",
			checked: state,
			onchange: (event) => {
				state.val = event.target.checked
			}
		}),
		_label,
	)
}

van.add(document.getElementById("root"), Page())
