import request from "./job.js"

export const convertSingle = async ({ speed, quality }) => {
	const file = await openFile()
	if (!file) return
	const buffer = await file.arrayBuffer()
	const msg = {
		buffer,
		speed,
		quality,
	}
	const res = await request(msg, [buffer])
	if (!res.avif) {
		return { message: "変換に失敗しました" }
	}
	await saveFile(res.avif, createName(file.name))
}

export const scanDir = async (filter) => {
	const handle = await window.showDirectoryPicker()
	const matched = []

	const filterFn = (name) => {
		const lower_name = name.toLowerCase()
		if (filter.jpg && lower_name.endsWith(".jpg")) return true
		if (filter.png && lower_name.endsWith(".png")) return true
		if (filter.gif && lower_name.endsWith(".gif")) return true
		if (filter.bmp && lower_name.endsWith(".bmp")) return true
		if (filter.webp && lower_name.endsWith(".webp")) return true
		return false
	}

	const scan = async (handle, path) => {
		for await (const entry_handle of handle.values()) {
			if (entry_handle instanceof FileSystemFileHandle) {
				if (filterFn(entry_handle.name)) {
					matched.push({
						path: [...path, entry_handle.name].join("/"),
						handle: entry_handle,
						parent_handle: handle,
					})
				}
			}
			if (entry_handle instanceof FileSystemDirectoryHandle) {
				await scan(entry_handle, [...path, entry_handle.name])
			}
		}
	}
	await scan(handle, [])

	return matched
}

export const convertMultiple = async ({ speed, quality, items, updateRow }) => {
	if (items.length === 0) {
		return { message: "変換対象がありません" }
	}

	await Promise.all(
		items.map(async item => {
			const file = await item.handle.getFile().catch(console.error)
			if (!file) {
				updateRow(item, { status: "ファイルの取得に失敗しました" })
				return
			}
			updateRow(item, { status: "変換中" })
			const buffer = await file.arrayBuffer()
			const source_length = buffer.byteLength
			const msg = {
				buffer,
				speed,
				quality,
			}
			const res = await request(msg, [buffer])
			if (!res.avif) {
				updateRow(item, { status: "変換に失敗しました" })
				return
			}
			try {
				const avif_name = createName(item.handle.name)
				const write_handle = await item.parent_handle.getFileHandle(avif_name, { create: true })
				await writeFile(write_handle, res.avif)
			} catch (err) {
				console.error(err)
				updateRow(item, { status: "保存に失敗しました" })
				return
			}
			updateRow(item, {
				status: "変換済み",
				source_size: source_length.toLocaleString() + " bytes",
				result_size: res.avif.byteLength.toLocaleString() + " bytes",
				elapsed: res.elapsed.toLocaleString() + " secs",
			})
		})
	)
}

const openFile = async () => {
	const [handle] = await window.showOpenFilePicker({
		types: [
			{
				description: "Images",
				accept: {
					"image/*": [".png", ".gif", ".jpeg", ".jpg", ".bmp", ".webp"],
				},
			},
		],
		excludeAcceptAllOption: true,
	}).catch(err => {
		console.error(err)
		return []
	})
	if (!handle) return

	return handle.getFile()
}

const saveFile = async (contents, name) => {
	const handle = await window.showSaveFilePicker({
		types: [
			{
				description: "Images",
				accept: {
					"image/*": [".avif"],
				},
			},
		],
		suggestedName: name,
		excludeAcceptAllOption: true,
	}).catch(err => {
		console.error(err)
	})
	if (!handle) return

	await writeFile(handle, contents)
}

const writeFile = async (handle, contents) => {
	const writable = await handle.createWritable()
	await writable.write(contents).finally(async () => {
		await writable.close()
	})
}

const createName = (filename) => {
	const parts = filename.split(".")
	if (parts.length > 1) {
		parts[parts.length - 1] = "avif"
	} else {
		parts.push("avif")
	}
	return parts.join(".")
}
