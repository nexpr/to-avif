
const p = ~~(navigator.hardwareConcurrency / 2) || 1
const job = Symbol("job")

const workers = []
const q = []

const onResult = (data, worker) => {
	const { resolve } = worker[job]
	worker[job] = null
	resolve(data.values)
	send()
}

let worker_ready = null
const prepareWorkers = async () => {
	if (worker_ready) return worker_ready

	const promises = []
	for (let i = 0; i < p; i++) {
		const worker = new Worker("./worker.js", { type: "module" })
		const { promise, resolve } = Promise.withResolvers()
		worker.addEventListener("message", (msg) => {
			const data = msg.data
			switch (data.type) {
				case "ready": {
					resolve()
					break
				}
				case "result": {
					onResult(data, worker)
					break
				}
			}
		})
		promises.push(promise)
		workers.push(worker)
	}

	worker_ready = Promise.all(promises)
	return worker_ready
}

const findIdleWorker = () => {
	for (const worker of workers) {
		if (!worker[job]) return worker
	}
}

const send = () => {
	if (q.length === 0) return

	const worker = findIdleWorker()
	if (!worker) return

	const { data, transfer, pwr } = q.shift()
	worker[job] = pwr
	worker.postMessage(data, transfer)
}

const request = async (data, transfer) => {
	const pwr = Promise.withResolvers()
	q.push({ data, transfer, pwr })

	await prepareWorkers()
	send()

	return pwr.promise
}

export default request
