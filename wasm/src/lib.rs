use wasm_bindgen::prelude::*;
use std::io::Cursor;
use image::codecs::avif::AvifEncoder;

#[wasm_bindgen]
pub fn convert(data: Vec<u8>, speed: u8, quality: u8) -> Result<Vec<u8>, JsError> {
    let result = image::load_from_memory(&data);
    let img = match result {
        Ok(img) => img,
        Err(_e) => return Err(JsError::new("Failed to load")),
    };

    let mut avif = Vec::new();
    let mut cursor = Cursor::new(&mut avif);
    let encoder = AvifEncoder::new_with_speed_quality(&mut cursor, speed, quality);
    let result = img.write_with_encoder(encoder);
    match result {
        Ok(()) => (),
        Err(_e) => return Err(JsError::new("Failed to convert")),
    };

    Ok(avif)
}
